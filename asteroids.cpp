#include <SDL2/SDL.h>
#include <time.h>

class Movable { public: SDL_FPoint pos, speed; };
class Model : public Movable { public: int num_lines, size; SDL_FPoint lines[10]; float angle; };
class Bullet : public Movable { public: int ttl; };
Model model[20];
Bullet bullet[20];
Bullet debris[20];
SDL_FPoint wl[10];

void create_rock(int size, int x = 0, int y = 0) {
    int i = 0; while (i<20 && model[i].num_lines > 0) { i++; }
    model[i].num_lines = 8;
    model[i].size = size; //900
    if (x == 0) { model[i].pos = { float(100+rand()%600), float(100+rand()%400) }; } else { model[i].pos = { float(x), float(y) }; }
    model[i].angle = rand()%360;
    model[i].speed = { (rand()%10) * (4-size) * 0.1f * SDL_cosf(model[i].angle*M_PI/180.0f), (rand()%10) * (3-size) * 0.1f * SDL_sinf(model[i].angle*M_PI/180.0f) };
    float da = 0.0f;
    for (int j=0; j<model[i].num_lines; j++) {
        model[i].lines[j] = { ((size*10)+rand()%(size*5)) * SDL_cosf(da*M_PI/180), ((size*10)+rand()%(size*5)) * -SDL_sinf(da*M_PI/180) };
        da += (360/model[i].num_lines);
    }
}

void move(Movable &mov) {
    mov.pos.x += mov.speed.x; mov.pos.y += mov.speed.y;
    if (mov.pos.x < -20) mov.pos.x += 840; if (mov.pos.x > 820) mov.pos.x -= 840;
    if (mov.pos.y < -20) mov.pos.y += 640; if (mov.pos.y > 620) mov.pos.y -= 640;
}

int main(int argc, char* argv[]) {
    srand(time(NULL));
    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_Window* sdlWindow = SDL_CreateWindow("ASTEROIDS", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
    SDL_Renderer* sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_PRESENTVSYNC);

    model[0].num_lines = 3; model[0].lines[0] = {15, 0}; model[0].lines[1] = {-10, 10}; model[0].lines[2] = {-10, -10}; model[0].pos = {400, 300};
    for (int i=1; i<4; i++) { create_rock(3); }

    SDL_Event sdlEvent;
    bool should_exit = false;
    while(!should_exit) {
        while(SDL_PollEvent(&sdlEvent)) {
            if (sdlEvent.type == SDL_QUIT) { should_exit = true; break; }
            if (sdlEvent.type == SDL_KEYDOWN) {
                if (sdlEvent.key.keysym.scancode == SDL_SCANCODE_SPACE) {
                    int i = 0;
                    while (i<20 && bullet[i].ttl > 0) i++;
                    bullet[i].pos = model[0].pos;
                    bullet[i].speed = { 8.0f * SDL_cosf(model[0].angle*M_PI/180.0f), -8.0f * SDL_sinf(model[0].angle*M_PI/180.0f) };
                    bullet[i].ttl = 100;
                }
            }
        }
        const Uint8* keys = SDL_GetKeyboardState(NULL);
        if (keys[SDL_SCANCODE_RIGHT]) model[0].angle-=10;
        if (keys[SDL_SCANCODE_LEFT]) model[0].angle+=10;
        if (keys[SDL_SCANCODE_UP]) { model[0].speed.x += 0.1f * SDL_cosf(model[0].angle*M_PI/180.0f); model[0].speed.y -= 0.1f * SDL_sinf(model[0].angle*M_PI/180.0f); }

        for (int i=0; i<20; i++) {
            if (model[i].num_lines > 0) { move(model[i]); if (i > 0) model[i].angle++; }
            if (debris[i].ttl > 0) { move(debris[i]); debris[i].ttl--; }
            if (bullet[i].ttl > 0) {
                move(bullet[i]);
                bullet[i].ttl--;
                for(int j=1; j<20; j++) if (model[j].num_lines > 0) {
                    if ( (bullet[i].pos.x-model[j].pos.x)*(bullet[i].pos.x-model[j].pos.x) + (bullet[i].pos.y-model[j].pos.y)*(bullet[i].pos.y-model[j].pos.y) < (100*model[j].size*model[j].size) ) {
                        bullet[i].ttl = 0;
                        if (model[j].size-1 > 0) { create_rock(model[j].size-1, model[j].pos.x, model[j].pos.y); create_rock(model[j].size-1, model[j].pos.x, model[j].pos.y); }
                        model[j].num_lines = 0;
                        for (int k=0; k<8; k++) {
                            int d = 0; while (d<20 && debris[d].ttl > 0) { d++; }
                            debris[d].pos = model[j].pos;
                            int angle = rand()%360; debris[d].speed = { 1.0f * SDL_cosf(angle*M_PI/180.0f), -1.0f * SDL_sinf(angle*M_PI/180.0f) };
                            debris[d].ttl = 30;
                        }
                        break;
                    }
                }
            }
        }
        SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255);
        SDL_RenderClear(sdlRenderer);
        SDL_SetRenderDrawColor(sdlRenderer, 255, 255, 255, 255);
        for (int i=0; i<20; i++) if (model[i].num_lines > 0) {
            for (int j=0; j<model[i].num_lines; j++) {
                wl[j].x = model[i].lines[j].x * SDL_cosf(model[i].angle*M_PI/180.0f) + model[i].lines[j].y * SDL_sinf(model[i].angle*M_PI/180.0f) + model[i].pos.x;
                wl[j].y = -model[i].lines[j].x * SDL_sinf(model[i].angle*M_PI/180.0f) + model[i].lines[j].y * SDL_cosf(model[i].angle*M_PI/180.0f) + model[i].pos.y;
            }
            wl[model[i].num_lines] = wl[0];
            SDL_RenderDrawLinesF(sdlRenderer, wl, model[i].num_lines+1);
            if (bullet[i].ttl > 0) { SDL_Rect rect { int(bullet[i].pos.x), int(bullet[i].pos.y), 2, 2}; SDL_RenderDrawRect(sdlRenderer, &rect); }
            if (debris[i].ttl > 0) { SDL_Rect rect { int(debris[i].pos.x), int(debris[i].pos.y), 2, 2}; SDL_RenderDrawRect(sdlRenderer, &rect); }
        }
        SDL_RenderPresent(sdlRenderer);
    }
    SDL_Quit();
    return 0;
}