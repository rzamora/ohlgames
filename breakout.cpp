#include <SDL2/SDL.h>

SDL_Event sdlEvent;
bool should_exit = false;
Uint8 bricks[10][8];
SDL_Point ball = {236, 592};
SDL_Point ball_dir = {3, -3};
int pala = 240-48;
Uint8 colors[4][2] {{255, 0}, {255, 128}, {0, 255}, {255, 255}};
Sint8 dirs[8][2] {{-7, -7}, {-5, -10}, {-3, -13}, {-1, -16}, {1, -16}, {3, -13}, {5, -10}, {7, -7}};
SDL_AudioSpec audioSpec{11025, AUDIO_U8, 1, 0, 512, 0, 0, NULL, NULL};
Uint8 bmp[84] = {0x42, 0x4D, 0x54, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x1E, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x12, 0x0B, 0x00, 0x00, 0x12, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x20, 0x20, 0x20, 0x00, 0xEB, 0xF3, 0xF9, 0xE4, 0xAA, 0x12, 0x69, 0xA4, 0xAB, 0xBF, 0xF9, 0xFD, 0xA8, 0x9B, 0x21, 0xB4, 0xEB, 0xFB, 0xFF, 0xFE, 0x00, 0x00};
int score = 0;
bool dead = true;
#define sound(a, b) if (!dead) { SDL_QueueAudio(sdlAudioDevice, &beeps[a], b); SDL_PauseAudioDevice(sdlAudioDevice, 0); }

int main(int argc, char* argv[]) {
    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_Window* sdlWindow = SDL_CreateWindow("BREAKOUT", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 480, 640, SDL_WINDOW_SHOWN);
    SDL_Renderer* sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_PRESENTVSYNC);
    SDL_Texture* sdlTexture = SDL_CreateTextureFromSurface(sdlRenderer, SDL_LoadBMP_RW(SDL_RWFromMem(bmp, 84), 1));
    SDL_AudioDeviceID sdlAudioDevice = SDL_OpenAudioDevice(NULL, 0, &audioSpec, NULL, 0);
    SDL_ShowCursor(0);
    SDL_CaptureMouse(SDL_TRUE);
    Uint8 beeps[8000];
    for (int i=0; i < 250;i++) { if ((i/2)%2==0) beeps[i]=0; else beeps[i]=128; }
    for (int i=250; i < 500;i++) { if ((i/3)%2==0) beeps[i]=0; else beeps[i]=128; }
    for (int i=500; i < 750;i++) { if ((i/4)%2==0) beeps[i]=0; else beeps[i]=128; }
    for (int i=750; i < 8000;i++) { beeps[i]=rand()%(255 - ((i-750)/29)); }
    while(!should_exit) {
        while(SDL_PollEvent(&sdlEvent)) {
            if (sdlEvent.type == SDL_QUIT) { should_exit = true; break; }
        }
        if ((SDL_GetMouseState(&pala, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)) && dead) {
            dead = false; score = 0; SDL_memset(bricks, 0, 80);
            ball = {236, 592};
            ball_dir = {7, -7};
            pala = 240-48;
        }
        pala = pala < 0 ? 0 : pala > 480-48 ? 480-48 : pala;
        ball.x += ball_dir.x;
        ball.y += ball_dir.y;
        if (ball.x > 480-8 || ball.x < 0) { ball_dir.x = -ball_dir.x; sound(250, 250); }
        if (ball.y < 0) { ball_dir.y = -ball_dir.y; sound(250, 250); }
        if (ball.y > 640-8) {
            if (dead) { sound(250, 250); } else { sound(750, 7250); }
            ball_dir.y = -ball_dir.y;
            dead = true;
        }
        if (!dead && ball.y > 600-8 && ball.y < 600+16 && ball.x > pala-8 && ball.x < pala+48) {
            ball_dir.x = dirs[SDL_min(7, SDL_max(0, (ball.x-pala+8)/7))][0];
            ball_dir.y = dirs[SDL_min(7, SDL_max(0, (ball.x-pala+8)/7))][1];
            sound(0, 250);
        }
        if (ball.y < 128 && bricks[ball.x/48][ball.y/16] == 0) {
            bricks[ball.x/48][ball.y/16] = 1; score++;
            ball_dir.y = -ball_dir.y;
            sound(500, 250);
        }
        SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255);
        SDL_RenderClear(sdlRenderer);
        char s[5]; SDL_itoa(score, s, 10);
        const int score_pos = (480 - (strlen(s) * 3 + strlen(s) - 1) * 32) / 2;
        for (int i=0; i < strlen(s); i++) {
            SDL_Rect src {(s[i]-48)*3, 0, 3, 5};
            SDL_Rect dst {score_pos + i*(32*4), 250, 3*32, 5*32};
            if (!dead) SDL_RenderCopy(sdlRenderer, sdlTexture, &src, &dst);
        }
        SDL_Rect rect {0, 0, 46, 14};
        for (rect.y = 1; rect.y < 129; rect.y += 16) {
            for (rect.x = 1; rect.x < 481; rect.x += 48) {
                if (bricks[(rect.x-1)/48][(rect.y-1)/16] == 0) {
                    SDL_SetRenderDrawColor(sdlRenderer, colors[rect.y/32][0], colors[rect.y/32][1], 0, 255);
                    SDL_RenderFillRect(sdlRenderer, &rect);
                }
            }
        }
        if (!dead) {
            SDL_SetRenderDrawColor(sdlRenderer, 0, 255, 255, 255);
            rect = {pala, 600, 48, 16};
            SDL_RenderFillRect(sdlRenderer, &rect);
        }
        SDL_SetRenderDrawColor(sdlRenderer, 255, 255, 255, 255);
        rect = {ball.x, ball.y, 8, 8};
        SDL_RenderFillRect(sdlRenderer, &rect);
        SDL_RenderPresent(sdlRenderer);
    }
    SDL_Quit();
    return 0;
}