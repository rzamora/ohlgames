#include <SDL2/SDL.h>

SDL_Event sdlEvent;
bool should_exit = false;
SDL_AudioSpec audioSpec{11025, AUDIO_U8, 1, 0, 512, 0, 0, NULL, NULL};
Uint8 bmp[132] { 0x42, 0x4D, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46, 0x00, 0x00, 0x00, 0x12, 0x0B, 0x00, 0x00, 0x12, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x82, 0x49, 0x24, 0x83, 0x82, 0x48, 0x24, 0x83, 0xF2, 0x48, 0x21, 0x9F, 0x82, 0x49, 0x20, 0x87, 0x9E, 0x49, 0x24, 0x9F, 0x82, 0x08, 0x24, 0x83, 0x82, 0x1C, 0x64, 0x83, 0x95, 0x51, 0xCD, 0x8A, 0x51, 0x57, 0xAA, 0xB9, 0x55, 0x53, 0xAA, 0x98, 0x75, 0x17, 0xAA, 0xBA, 0x93, 0x51, 0x9A, 0x89, 0x14, 0x0C, 0x06, 0x1B, 0x55, 0xED, 0x96, 0x5B, 0x54, 0x40, 0x06, 0x03, 0x57, 0x64, 0xDE, 0x4B, 0x14, 0x04, 0x00, 0x03, 0x00, 0x00 };
Uint8 mapa[40][30];
SDL_Point head {20, 15}, tail {20, 15};
int wait_time = 0;
Uint8 last_dir = 0;
int count = -1;
#define sound(a, b) SDL_QueueAudio(sdlAudioDevice, &beeps[a], b); SDL_PauseAudioDevice(sdlAudioDevice, 0);

void new_apple() {
    int x, y;
    do { x = rand()%40; y = rand()%30; } while( mapa[x][y] != 0);
    mapa[x][y] = 255;
}

int main(int argc, char* argv[]) {
    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_Window* sdlWindow = SDL_CreateWindow("SNAKE", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_SHOWN);
    SDL_Renderer* sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_PRESENTVSYNC);
    SDL_RenderSetLogicalSize(sdlRenderer, 40, 30);
    SDL_Texture* sdlTexture = SDL_CreateTextureFromSurface(sdlRenderer, SDL_LoadBMP_RW(SDL_RWFromMem(bmp, 132), 1));
    SDL_SetTextureBlendMode(sdlTexture, SDL_BLENDMODE_ADD);
    SDL_AudioDeviceID sdlAudioDevice = SDL_OpenAudioDevice(NULL, 0, &audioSpec, NULL, 0);
    Uint8 beeps[8000];
    for (int i=0; i < 250;i++) { if ((i/2)%2==0) beeps[i]=0; else beeps[i]=128; }
    for (int i=250; i < 7500;i++) { beeps[i]=rand()%(255 - ((i-250)/29)); }

    while (!should_exit) {
        while(SDL_PollEvent(&sdlEvent)) {
            if (sdlEvent.type == SDL_QUIT) { should_exit = true; break; }
        }
        const Uint8* keys = SDL_GetKeyboardState(NULL);

        SDL_SetRenderDrawColor(sdlRenderer, 24, 29, 25, 255);
        SDL_RenderClear(sdlRenderer);

        SDL_Rect rect[2] = { {0, 5, 32, 5}, {4, 10, 32, 5} };
        switch (count) {
            case -1:
                rect[0] = {0, 10, 30, 7};
                rect[1] = {5, 10, 30, 7};
            case 0:
                SDL_RenderCopy(sdlRenderer, sdlTexture, &rect[0], &rect[1]);
                if (keys[SDL_SCANCODE_SPACE] || keys[SDL_SCANCODE_RETURN]) {
                    count = 599;
                    head = tail = {20, 15};
                    SDL_memset(mapa, 0, 40*30);
                    mapa[head.x][head.y] = SDL_SCANCODE_RIGHT + rand()%4;
                    new_apple();
                }
                break;
            default:
                count--;
                if (++wait_time == 4) {
                    wait_time = 0;
                    last_dir = mapa[head.x][head.y];
                    if (mapa[head.x][head.y] == SDL_SCANCODE_UP) { head.y--; if (head.y < 0) head.y = 29; }
                    else if (mapa[head.x][head.y] == SDL_SCANCODE_RIGHT) { head.x++; if (head.x == 40) head.x = 0; }
                    else if (mapa[head.x][head.y] == SDL_SCANCODE_DOWN) { head.y++; if (head.y == 30) head.y = 0; }
                    else if (mapa[head.x][head.y] == SDL_SCANCODE_LEFT) { head.x--; if (head.x < 0) head.x = 39; }

                    if (mapa[head.x][head.y] != 255) {
                        if (mapa[head.x][head.y] != 0) { sound(250, 7250); count = 0; }
                        if (mapa[tail.x][tail.y] == SDL_SCANCODE_UP) { mapa[tail.x][tail.y] = 0; tail.y--; if (tail.y < 0) tail.y = 29; }
                        else if (mapa[tail.x][tail.y] == SDL_SCANCODE_RIGHT) { mapa[tail.x][tail.y] = 0; tail.x++; if (tail.x == 40) tail.x = 0; }
                        else if (mapa[tail.x][tail.y] == SDL_SCANCODE_DOWN) { mapa[tail.x][tail.y] = 0; tail.y++; if (tail.y == 30) tail.y = 0; }
                        else if (mapa[tail.x][tail.y] == SDL_SCANCODE_LEFT) { mapa[tail.x][tail.y] = 0; tail.x--; if (tail.x < 0) tail.x = 39; }
                    } else {
                        sound(0, 250); count = 599;
                        new_apple();
                    }
                    mapa[head.x][head.y] = last_dir;
                }
                if (keys[SDL_SCANCODE_UP] && last_dir != SDL_SCANCODE_DOWN) mapa[head.x][head.y] = SDL_SCANCODE_UP;
                else if (keys[SDL_SCANCODE_RIGHT] && last_dir != SDL_SCANCODE_LEFT) mapa[head.x][head.y] = SDL_SCANCODE_RIGHT;
                else if (keys[SDL_SCANCODE_DOWN] && last_dir != SDL_SCANCODE_UP) mapa[head.x][head.y] = SDL_SCANCODE_DOWN;
                else if (keys[SDL_SCANCODE_LEFT] && last_dir != SDL_SCANCODE_RIGHT) mapa[head.x][head.y] = SDL_SCANCODE_LEFT;

                if (count > 180 || ((count/8) % 2) == 0) {
                    SDL_Rect rect[2] = { {3*(count/60), 0, 3, 5}, {19, 2, 3, 5} };
                    SDL_SetTextureColorMod(sdlTexture, 130, 210, 225); SDL_RenderCopy(sdlRenderer, sdlTexture, &rect[0], &rect[1]); SDL_SetTextureColorMod(sdlTexture, 251, 249, 254);
                }

                SDL_Rect rect = {0, 0, 1, 1};
                for (rect.y = 0; rect.y < 30; rect.y++) {
                    for (rect.x = 0; rect.x < 40; rect.x++) {
                        if (mapa[rect.x][rect.y] == 255) { SDL_SetRenderDrawColor(sdlRenderer, 149, 58, 75, 255); SDL_RenderFillRect(sdlRenderer, &rect); }
                        else if (mapa[rect.x][rect.y] != 0) { SDL_SetRenderDrawColor(sdlRenderer, 251, 249, 254, 255); SDL_RenderFillRect(sdlRenderer, &rect); }
                    }
                }
        }
        SDL_RenderPresent(sdlRenderer);
    }
    SDL_Quit();
    return 0;
}