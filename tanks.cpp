#include <SDL2/SDL.h>

struct Tank { float x, y, angle; SDL_Scancode up, down, left, right, fire; int shock, score; };
struct Bullet { float x, y, dx, dy; int ttl; };
SDL_Event sdlEvent;
bool should_exit = false;
SDL_AudioSpec audioSpec{11025, AUDIO_U8, 1, 0, 512, 0, 0, NULL, NULL};
Uint8 map[24][40] = { {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1} };
Uint8 bmp[112] = {0x42, 0x4D, 0x70, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x1E, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x32, 0x00, 0x00, 0x00, 0x12, 0x0B, 0x00, 0x00, 0x12, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x99, 0x27, 0x24, 0x80, 0x98, 0x20, 0x20, 0xF0, 0x99, 0x22, 0x21, 0x80, 0x99, 0x20, 0x24, 0x80, 0x08, 0x20, 0x24, 0x9C, 0x0C, 0x65, 0x24, 0x80, 0xFF, 0xFD, 0xFF, 0xFC, 0x14, 0x0C, 0x06, 0x18, 0x55, 0xED, 0x96, 0x58, 0x54, 0x40, 0x06, 0x00, 0x57, 0x64, 0xDE, 0x48, 0x14, 0x04, 0x00, 0x00, 0x00, 0x00};
Tank tank[2] {{572, 258, 270, SDL_SCANCODE_UP, SDL_SCANCODE_DOWN, SDL_SCANCODE_LEFT, SDL_SCANCODE_RIGHT, SDL_SCANCODE_RCTRL, 0, 0}, {40, 258, 90, SDL_SCANCODE_W, SDL_SCANCODE_S, SDL_SCANCODE_A, SDL_SCANCODE_D, SDL_SCANCODE_LCTRL, 0, 0}};
Bullet bullet[2] { {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0} };
Uint8 colors[2][3] { {130, 210, 225}, {149, 58, 75} };
#define sound(a, b) SDL_QueueAudio(sdlAudioDevice, &beeps[a], b); SDL_PauseAudioDevice(sdlAudioDevice, 0);

int main(int argc, char* argv[]) {
    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_Window* sdlWindow = SDL_CreateWindow("TANKS", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_SHOWN);
    SDL_Renderer* sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_PRESENTVSYNC);
    SDL_Texture* sdlTexture = SDL_CreateTextureFromSurface(sdlRenderer, SDL_LoadBMP_RW(SDL_RWFromMem(bmp, 112), 1));
    SDL_AudioDeviceID sdlAudioDevice = SDL_OpenAudioDevice(NULL, 0, &audioSpec, NULL, 0);

    Uint8 beeps[8000];
    for (int i=0; i < 250;i++) { beeps[i]=rand()%255; }
    for (int i=250; i < 500;i++) { if ((i/3)%2==0) beeps[i]=0; else beeps[i]=128; }
    for (int i=500; i < 750;i++) { if ((i/4)%2==0) beeps[i]=0; else beeps[i]=128; }
    for (int i=750; i < 8000;i++) { beeps[i]=rand()%(255 - ((i-750)/29)); }

    while (!should_exit) {
        while(SDL_PollEvent(&sdlEvent)) {
            if (sdlEvent.type == SDL_QUIT) { should_exit = true; break; }
        }
        const Uint8* keys = SDL_GetKeyboardState(NULL);
        if (keys[SDL_SCANCODE_ESCAPE]) { should_exit = true; break; }

        for (int i=0; i<2; i++) {
            if (tank[i].shock == 0) {
                if (keys[tank[i].right]) {sound(0, 16); tank[i].angle += 4; if (tank[i].angle > 360) tank[i].angle -= 360;}
                if (keys[tank[i].left]) {sound(0, 16); tank[i].angle -= 4; if (tank[i].angle < 0) tank[i].angle += 360;}
                if (keys[tank[i].up]) { sound(0, 16);
                    tank[i].x += SDL_sinf(tank[i].angle*M_PI/180);
                    if ((map[int(tank[i].y-80)/16][int(tank[i].x)/16] == 1) || (map[int(tank[i].y-80)/16][int(tank[i].x+27)/16] == 1) || (map[int(tank[i].y-52)/16][int(tank[i].x)/16] == 1) || (map[int(tank[i].y-52)/16][int(tank[i].x+27)/16] == 1)) {
                        tank[i].x -= SDL_sinf(tank[i].angle*M_PI/180);
                    }
                    tank[i].y -= SDL_cosf(tank[i].angle*M_PI/180);
                    if ((map[int(tank[i].y-80)/16][int(tank[i].x)/16] == 1) || (map[int(tank[i].y-80)/16][int(tank[i].x+27)/16] == 1) || (map[int(tank[i].y-52)/16][int(tank[i].x)/16] == 1) || (map[int(tank[i].y-52)/16][int(tank[i].x+27)/16] == 1)) {
                        tank[i].y += SDL_cosf(tank[i].angle*M_PI/180);
                    }
                }
                if (keys[tank[i].down]) {sound(0, 16);
                    tank[i].x -= SDL_sinf(tank[i].angle*M_PI/180);
                    if ((map[int(tank[i].y-80)/16][int(tank[i].x)/16] == 1) || (map[int(tank[i].y-80)/16][int(tank[i].x+27)/16] == 1) || (map[int(tank[i].y-52)/16][int(tank[i].x)/16] == 1) || (map[int(tank[i].y-52)/16][int(tank[i].x+27)/16] == 1)) {
                        tank[i].x += SDL_sinf(tank[i].angle*M_PI/180);
                    }
                    tank[i].y += SDL_cosf(tank[i].angle*M_PI/180);
                    if ((map[int(tank[i].y-80)/16][int(tank[i].x)/16] == 1) || (map[int(tank[i].y-80)/16][int(tank[i].x+27)/16] == 1) || (map[int(tank[i].y-52)/16][int(tank[i].x)/16] == 1) || (map[int(tank[i].y-52)/16][int(tank[i].x+27)/16] == 1)) {
                        tank[i].y -= SDL_cosf(tank[i].angle*M_PI/180);
                    }
                }
                if (keys[tank[i].fire] && bullet[i].ttl == 0) {sound(250, 250); bullet[i].ttl=5; bullet[i].x = tank[i].x+14; bullet[i].y = tank[i].y+14; bullet[i].dx=SDL_sinf(tank[i].angle*M_PI/180); bullet[i].dy=-SDL_cosf(tank[i].angle*M_PI/180);}
            } else {
                tank[i].shock--;
                tank[i].angle += 20; if (tank[i].angle > 360) tank[i].angle -= 360;
            }
            if (bullet[i].ttl > 0) {
                bullet[i].x += 4*bullet[i].dx;
                if (map[int(bullet[i].y-80)/16][int(bullet[i].x)/16] == 1) { bullet[i].dx = -bullet[i].dx; bullet[i].x += 4*bullet[i].dx; sound(500, 250); bullet[i].ttl--;}
                bullet[i].y += 4*bullet[i].dy;
                if (map[int(bullet[i].y-80)/16][int(bullet[i].x)/16] == 1) { bullet[i].dy = -bullet[i].dy; bullet[i].y += 4*bullet[i].dy; sound(500, 250); bullet[i].ttl--;}

                if (tank[1-i].shock == 0 && bullet[i].x > tank[1-i].x && bullet[i].x < tank[1-i].x+27 && bullet[i].y > tank[1-i].y && bullet[i].y < tank[1-i].y+27) { sound(750, 7250); bullet[i].ttl = 0; tank[1-i].shock = 50; tank[i].score++; }
            }
        }
        SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255);
        SDL_RenderClear(sdlRenderer);
        for (int i=0; i<2; i++) {
            SDL_Rect src {11, 5, 7, 7};
            SDL_Rect dst {int(tank[i].x), int(tank[i].y), 28, 28}; SDL_SetTextureColorMod(sdlTexture, colors[i][0], colors[i][1], colors[i][2]);
            SDL_RenderCopyEx(sdlRenderer, sdlTexture, &src, &dst, tank[i].angle, NULL, SDL_FLIP_NONE);
            SDL_SetRenderDrawColor(sdlRenderer, colors[i][0], colors[i][1], colors[i][2], 255);
            if (bullet[i].ttl != 0) { dst = {int(bullet[i].x)-2, int(bullet[i].y)-2, 4, 4}; SDL_RenderFillRect(sdlRenderer, &dst); }
            src = {tank[i].score*3, 0, 3, 5}; dst = {80+(1-i)*440, 16, 40, 40};
            SDL_RenderCopy(sdlRenderer, sdlTexture, &src, &dst);
        }

        SDL_SetRenderDrawColor(sdlRenderer, 251, 249, 254, 255);
        for (int y=0; y<24; y++) {
            for (int x=0; x<40; x++) {
                if (map[y][x] == 1) { SDL_Rect dst = {x*16, 80+y*16, 16, 16}; SDL_RenderFillRect(sdlRenderer, &dst); }
            }
        }
        SDL_RenderPresent(sdlRenderer);
    }
    SDL_Quit();
    return 0;
}