#include <SDL2/SDL.h>

SDL_Renderer* sdlRenderer = NULL;
SDL_Texture* sdlTexture = NULL;
bool should_exit = false;
SDL_Event sdlEvent;
SDL_AudioSpec audioSpec{11025, AUDIO_U8, 1, 0, 512, 0, 0, NULL, NULL};
Uint8 bmp[160] {0x42, 0x4D, 0xA0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x62, 0x00, 0x00, 0x00, 0x12, 0x0B, 0x00, 0x00, 0x12, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x15, 0x51, 0x8D, 0x8A, 0x55, 0x57, 0xAA, 0xB9, 0x51, 0x53, 0xAA, 0x98, 0x75, 0x17, 0xAA, 0xBA, 0x11, 0x51, 0x8A, 0x88, 0x3E, 0x04, 0xE4, 0x0F, 0x3E, 0x04, 0xE4, 0x0F, 0x3E, 0x64, 0xE4, 0xCF, 0x3E, 0x64, 0xC4, 0xCF, 0x02, 0x64, 0x84, 0x8F, 0x02, 0x64, 0x04, 0xFF, 0x32, 0x64, 0x24, 0xFF, 0x32, 0x64, 0x64, 0xCF, 0x02, 0x04, 0xE4, 0x0F, 0x02, 0x04, 0xE4, 0x0F, 0x14, 0x0C, 0x06, 0x1B, 0x55, 0xED, 0x96, 0x5B, 0x54, 0x40, 0x06, 0x03, 0x57, 0x64, 0xDE, 0x4B, 0x14, 0x04, 0x00, 0x03, 0x00, 0x00};
int player1_y = 260, player2_y = 260, player1_score = 0, player2_score = 0, ball_x, ball_y, ball_dx, ball_dy = 4;
bool wait = true;
#define sound(a, b) SDL_QueueAudio(sdlAudioDevice, &beeps[a], b); SDL_PauseAudioDevice(sdlAudioDevice, 0);

void Draw(int x, int y, int sx, int sy, int w, int h) {
	SDL_Rect rects[2] {{sx, sy, w, h}, {x, y, w*10, h*10}};
	SDL_RenderCopy(sdlRenderer, sdlTexture, &rects[0], &rects[1]);
}

int main(int argc, char* argv[]) {
	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Window* sdlWindow = SDL_CreateWindow("PONG", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
	sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_PRESENTVSYNC);
	sdlTexture = SDL_CreateTextureFromSurface(sdlRenderer, SDL_LoadBMP_RW(SDL_RWFromMem(bmp, 160), 1));
    SDL_SetTextureBlendMode(sdlTexture, SDL_BLENDMODE_ADD);
    SDL_AudioDeviceID sdlAudioDevice = SDL_OpenAudioDevice(NULL, 0, &audioSpec, NULL, 0);
    Uint8 beeps[8000];
    for (int i=0; i < 250;i++) { if ((i/2)%2==0) beeps[i]=0; else beeps[i]=128; }
    for (int i=250; i < 500;i++) { if ((i/3)%2==0) beeps[i]=0; else beeps[i]=128; }
    for (int i=500; i < 8000;i++) { beeps[i]=rand()%(255 - ((i-500)/30)); }

	while (!should_exit) {
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT) { should_exit = true; break; }
		}
		SDL_SetTextureColorMod(sdlTexture, 251, 249, 254);
		SDL_SetRenderDrawColor(sdlRenderer, 24, 29, 25, 255);
		SDL_RenderClear(sdlRenderer);
		if (wait && player1_score == 0 && player2_score == 0) {
			Draw(260, 190, 0, 5, 28, 10);
		} else {
			for (int i = 0; i < 600; i += 40) Draw(395, i, 0, 0, 1, 2);
		}
		Draw(190, 40, 3 * player1_score, 0, 3, 5);
		Draw(590, 40, 3 * player2_score, 0, 3, 5);

		Draw(80, player1_y, 0, 0, 1, 8);
		Draw(720, player2_y, 0, 0, 1, 8);
		Draw(ball_x, ball_y, 0, 0, 1, 1);

		const Uint8* state = SDL_GetKeyboardState(NULL);
		if (state[SDL_SCANCODE_Q] && player1_y > 0) player1_y -= 20;
		if (state[SDL_SCANCODE_A] && player1_y < 520) player1_y += 20;
		if (state[SDL_SCANCODE_UP] && player2_y > 0) player2_y -= 20;
		if (state[SDL_SCANCODE_DOWN] && player2_y < 520) player2_y += 20;
		if (wait && (state[SDL_SCANCODE_RETURN] || state[SDL_SCANCODE_SPACE])) { ball_x = 395; ball_y = 295; ball_dx = ball_dx < 0 ? -4 : 4; wait = false; }

		if (!wait) {
			ball_x += ball_dx; ball_y += ball_dy;
			if (ball_y < 0 || ball_y > 590) { ball_dy = -ball_dy; sound(0, 250); }
			if (ball_x < 0) { player2_score++; wait = true; sound(500, 7500); }
			if (ball_x > 790) { player1_score++; wait = true; sound(500, 7500); }
			if (ball_dx < 0 && int(ball_x) <= 90 && int(ball_x) >= 80 && ball_y >= player1_y && ball_y <= player1_y + 80) { ball_dx = -ball_dx; if (ball_dx <= 10) ball_dx = ball_dx + 1; sound(250, 250); }
			if (ball_dx > 0 && int(ball_x) >= 710 && int(ball_x) <= 720 && ball_y >= player2_y && ball_y <= player2_y + 80) { ball_dx = -ball_dx; if (ball_dx >= -10) ball_dx = ball_dx - 1; sound(250, 250); }
		}
		SDL_RenderPresent(sdlRenderer);
	}
	SDL_Quit();
	return 0;
}